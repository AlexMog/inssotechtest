FROM openjdk:11-jdk-alpine AS java-builder
ADD . .
RUN chmod +x ./mvnw && ./mvnw -q -Dmaven.test.skip=true clean install -Pprod

FROM openjdk:11-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY --from=java-builder /target/*.jar app.jar
EXPOSE 5000
ENTRYPOINT ["java", "-jar", "/app.jar"]
