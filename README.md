# Insso Tech Test
Welcome to the Insso Tech Test api.

This project uses Spring Boot Framework under the hood.

**Swagger documentation can be found on the /swagger-ui.html
endpoint when starting the application.**

# Run locally
The project can be run locally using the following command:
```bash
mvn spring-boot:run
```

You will then be able to access the api using the following url: 
`http://localhost:8080/`.

# Run tests
## Dependencies
Running tests on this project requires the following dependencies installed:
* Java 11+
* Maven

## Run
Simply run the tests using the following command:
```bash
mvn test
```

# Compile the project
To compile the project as an executable, run the following command:
```bash
./mvnw -q -Dmaven.test.skip=true clean install
```

## Using Docker
A Docker image is available to run the project.  
To build the image, use the following commands:
```bash
docker build -t test .
docker run -d -p 5000:5000 test
```

The server will start and should be available on the port 5000.

## Pagination
The pagination uses Spring HATEOAS pagination system.  
The pagination is limited to 100 elements per page and has a default value of 20 elements.  
HATEOAS Pagination allows for data Sorting, paging and sorting usage documentation can
be found [here](https://docs.spring.io/spring-data/rest/docs/current/reference/html/#paging-and-sorting).

## Data retrieval
Data retrieval endpoints (GET) follows a pattern to allow the retrieval of multiple elements
using an array of ids.  
This allows for simple recuperation of elements by their ids instead of doing a per-resource call.  
Example:
```
GET /messages?id=1&id=2&id=3...
```
insead of
```
GET /messages/1
GET /messages/2
GET /messages/3
```

The ids array follows the same limitations as the pagination system: 100 elements max.

## Metrics
Metrics are available using the Prometheus format on the `/metrics` endpoint. (prometheus format)