package gg.mog.inssotechtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InssoTechTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(InssoTechTestApplication.class, args);
    }

}
