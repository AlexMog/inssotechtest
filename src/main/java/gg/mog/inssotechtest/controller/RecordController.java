package gg.mog.inssotechtest.controller;

import gg.mog.inssotechtest.model.db.Record;
import gg.mog.inssotechtest.model.dto.UpdateRecordDto;
import gg.mog.inssotechtest.service.RecordService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;
import java.util.UUID;

@RestController
@RequestMapping("records")
@Validated
public class RecordController {
    private final RecordService recordService;

    public RecordController(@Autowired RecordService recordService) {
        this.recordService = recordService;
    }

    @Operation(
            description = "Retrieve records from ids or client name",
            parameters = {
                    @Parameter(name = "id", description = "Messages ids array"),
                    @Parameter(name = "client_name", description = "The client name to retrieve records from")
            },
            responses = {
                    @ApiResponse(responseCode = "200", content = {
                            @Content(array = @ArraySchema(
                                    schema = @Schema(implementation = Record.class)
                            ))
                    })
            }
    )
    @GetMapping
    public ResponseEntity<Iterable<Record>> getRecords(
            @RequestParam(value = "id", required = false) @Size(min = 1, max = 100) Set<UUID> ids,
            @RequestParam(value = "client_name", required = false) @Size(min = 1) String client,
            Pageable pageable
    ) {
        if (ids != null) {
            return ResponseEntity.ok(recordService.get(ids));
        }

        if (client != null) {
            return ResponseEntity.ok(recordService.findByClient(client, pageable));
        }

        return ResponseEntity.ok(recordService.findAll(pageable));
    }

    @Operation(
            description = "Creates new record"
    )
    @PostMapping
    public ResponseEntity<Record> createRecord(@RequestBody @Valid @NotNull UpdateRecordDto body) {
        return ResponseEntity.ok(recordService.create(body));
    }

    @Operation(
            description = "Updates a record"
    )
    @PutMapping("{id}")
    public ResponseEntity<Record> updateRecord(
            @PathVariable("id") UUID id,
            @RequestBody @Valid @NotNull UpdateRecordDto body
    ) {
        return ResponseEntity.ok(recordService.update(id, body));
    }

    @Operation(
            description = "Deletes a record"
    )
    @DeleteMapping("{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteRecord(@PathVariable("id") UUID id) {
        recordService.delete(id);
    }
}
