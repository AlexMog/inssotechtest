package gg.mog.inssotechtest.controller;

import gg.mog.inssotechtest.model.db.Message;
import gg.mog.inssotechtest.model.dto.UpdateMessageDto;
import gg.mog.inssotechtest.service.MessageService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;
import java.util.UUID;

@RestController
@RequestMapping("messages")
@Validated
public class MessageController {
    private final MessageService messageService;

    public MessageController(@Autowired MessageService messageService) {
        this.messageService = messageService;
    }

    @Operation(
            description = "Retrieve messages from ids",
            parameters = {
                    @Parameter(name = "id", description = "Messages ids array")
            },
            responses = {
                @ApiResponse(responseCode = "200", content = {
                    @Content(array = @ArraySchema(
                            schema = @Schema(implementation = Message.class)
                    ))
            })
        }
    )
    @GetMapping
    public Iterable<Message> getMessages(@RequestParam("id") @Size(min = 1, max = 100) Set<UUID> ids) {
        return messageService.get(ids);
    }

    @Operation(
            description = "Creates new message"
    )
    @PostMapping
    public ResponseEntity<Message> createMessage(@RequestBody @Valid @NotNull UpdateMessageDto body) {
        return ResponseEntity.ok(messageService.create(body));
    }

    @Operation(
            description = "Updates a message"
    )
    @PutMapping("{id}")
    public ResponseEntity<Message> updateMessage(
            @PathVariable("id") UUID id,
            @RequestBody @Valid @NotNull UpdateMessageDto body
    ) {
        return ResponseEntity.ok(messageService.update(id, body));
    }

    @DeleteMapping("{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteMessage(@PathVariable("id") UUID id) {
        messageService.delete(id);
    }
}
