package gg.mog.inssotechtest.repository;

import gg.mog.inssotechtest.model.db.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface MessageRepository extends JpaRepository<Message, UUID> {
    Page<Message> findByAuthor(String author, Pageable pageable);
}
