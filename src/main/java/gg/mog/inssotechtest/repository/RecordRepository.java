package gg.mog.inssotechtest.repository;

import gg.mog.inssotechtest.model.db.Record;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface RecordRepository extends JpaRepository<Record, UUID> {
    Page<Record> findByClientName(String client, Pageable pageable);
}
