package gg.mog.inssotechtest.model.dto;

import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.List;

@Data
@Builder
public class ApiError {
    private HttpStatus status;
    private String message;
    private List<FieldError> fieldErrors;
    private List<String> globalErrors;

    @Data
    @Builder
    public static class FieldError {
        private String message;
        private String field;
    }
}
