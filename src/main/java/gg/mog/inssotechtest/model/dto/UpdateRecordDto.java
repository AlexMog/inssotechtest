package gg.mog.inssotechtest.model.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;

@Data
@Builder
public class UpdateRecordDto {
    private String reference;
    @NotNull
    private Set<UUID> messages;
    @NotNull
    @NotEmpty
    private String clientName;
}
