package gg.mog.inssotechtest.model.dto;

import gg.mog.inssotechtest.model.db.Message;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class UpdateMessageDto {
    @NotNull
    @NotEmpty
    private String author;
    @NotNull
    @NotEmpty
    private String content;
    @NotNull
    private Message.Channel channel;
}
