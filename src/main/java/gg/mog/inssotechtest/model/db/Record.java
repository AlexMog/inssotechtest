package gg.mog.inssotechtest.model.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@Entity
@Table(indexes = {
        @Index(columnList = "clientName")
})
public class Record {
    // Using UUID instead of auto-increment to avoid simple discovery of messages
    // (this does not replace an auth/permission system tho)
    // It is required by the RGPD. Your ids must not be simply discovered.
    @Id
    @GeneratedValue
    private UUID id;
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    // Here, for simplicity reasons, I am using Joins for messages fetching
    // But, for scaling reasons, it is better to use a list of message Ids
    // and use the front or CQRS systems to access the resource as a separated resource instead of an integrated one
    // -it also allows for simple micro-service conversion if no joins are used-
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    private Set<Message> messages;
    private String reference;
    private String clientName;

    public Record() {}
}
