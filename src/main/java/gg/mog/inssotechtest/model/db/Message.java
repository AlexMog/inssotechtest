package gg.mog.inssotechtest.model.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@Entity
@Table(indexes = {
        @Index(columnList = "author")
})
public class Message {
    // Using UUID instead of auto-increment to avoid simple discovery of messages
    // (this does not replace an auth/permission system tho)
    // It is required by the RGPD. Your ids must not be simply discovered.
    @Id
    @GeneratedValue
    private UUID id;
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    private String author;
    private String content;
    private Channel channel;

    public Message() {}

    public enum Channel {
        EMAIL,
        SMS,
        FACEBOOK,
        TWITTER
    }
}
