package gg.mog.inssotechtest.service;

import gg.mog.inssotechtest.model.db.Message;
import gg.mog.inssotechtest.model.db.Record;
import gg.mog.inssotechtest.model.dto.UpdateRecordDto;
import gg.mog.inssotechtest.repository.RecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class RecordService {
    private final RecordRepository recordRepository;
    private final MessageService messageService;

    public RecordService(
            @Autowired RecordRepository recordRepository,
            @Autowired MessageService messageService
    ) {
        this.messageService = messageService;
        this.recordRepository = recordRepository;
    }

    public Record create(UpdateRecordDto record) {
        return update(null, record);
    }

    public Record update(UUID id, UpdateRecordDto record) {
        if (id != null && !recordRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Record not found");
        }

        Set<Message> messages = null;
        if (record.getMessages().size() > 0) {
            messages = StreamSupport.stream(messageService.get(
                    record.getMessages()
            ).spliterator(), false).collect(Collectors.toSet());
            if (messages.size() != record.getMessages().size()) {
                throw new ResponseStatusException(HttpStatus.CONFLICT, "One of the message haven't been found");
            }
        }

        return recordRepository.save(
                Record.builder()
                        .id(id)
                        .messages(messages == null ? new HashSet<Message>(0) : messages)
                        .reference(record.getReference())
                        .clientName(record.getClientName())
                        .build()
        );
    }

    public Iterable<Record> get(Set<UUID> ids) {
        return recordRepository.findAllById(ids);
    }

    public void delete(UUID id) {
        recordRepository.findById(id).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "Record not found"));
        recordRepository.deleteById(id);
    }

    public Page<Record> findByClient(String client, Pageable pageable) {
        return recordRepository.findByClientName(client, pageable);
    }

    public Page<Record> findAll(Pageable pageable) {
        return recordRepository.findAll(pageable);
    }
}
