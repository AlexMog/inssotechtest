package gg.mog.inssotechtest.service;

import gg.mog.inssotechtest.model.db.Message;
import gg.mog.inssotechtest.model.db.Record;
import gg.mog.inssotechtest.model.dto.UpdateMessageDto;
import gg.mog.inssotechtest.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class MessageService {
    private final MessageRepository messageRepository;

    public MessageService(@Autowired MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public Iterable<Message> get(Set<UUID> messages) {
        return messageRepository.findAllById(messages);
    }

    public Message create(UpdateMessageDto message) {
        return update(null, message);
    }

    public Message update(UUID id, UpdateMessageDto message) {
        if (id != null && !messageRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Message not found");
        }

        return messageRepository.save(
                Message.builder()
                        .id(id)
                        .author(message.getAuthor())
                        .channel(message.getChannel())
                        .content(message.getContent())
                        .build()
        );
    }

    public void delete(UUID id) {
        messageRepository.findById(id).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "Record not found"));
        messageRepository.deleteById(id);
    }

    public Page<Message> findByAuthor(String author, Pageable pageable) {
        return messageRepository.findByAuthor(author, pageable);
    }
}
