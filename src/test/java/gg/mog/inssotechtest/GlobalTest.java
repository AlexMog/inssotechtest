package gg.mog.inssotechtest;

import gg.mog.inssotechtest.model.db.Message;
import gg.mog.inssotechtest.model.db.Record;
import gg.mog.inssotechtest.model.dto.UpdateMessageDto;
import gg.mog.inssotechtest.model.dto.UpdateRecordDto;
import gg.mog.inssotechtest.repository.RecordRepository;
import gg.mog.inssotechtest.service.MessageService;
import gg.mog.inssotechtest.service.RecordService;
import org.junit.jupiter.api.*;
import org.mockito.internal.util.collections.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest
class GlobalTest {
    @Autowired
    RecordService recordService;
    @Autowired
    MessageService messageService;
    @Autowired
    RecordRepository recordRepository;
    static final Set<Message> messages = new HashSet<>();
    static Record record;

    @Order(0)
    @Test
    public void createJeremyDurandMessage() {
        messages.add(messageService.create(UpdateMessageDto.builder()
                .content("Bonjour, j’ai un problème avec mon nouveau téléphone")
                .channel(Message.Channel.SMS)
                .author("Jérémie Durand")
                .build()));
        Assertions.assertEquals(1, messageService.findByAuthor("Jérémie Durand", Pageable.unpaged())
                .getNumberOfElements());
    }

    @Order(1)
    @Test
    public void createJeremyDurandRecord() {
        record = recordService.create(UpdateRecordDto.builder()
                .messages(messages.stream().map(Message::getId).collect(Collectors.toSet()))
                .clientName("Jérémie Durand")
                .build());
        Assertions.assertEquals(1, recordService.findByClient("Jérémie Durand", Pageable.unpaged())
                .getNumberOfElements());
    }

    @Order(2)
    @Test
    public void createSoniaValentinMessage() {
        messages.add(messageService.create(UpdateMessageDto.builder()
                .content("Je suis Sonia, et je vais mettre tout en œuvre pour vous aider. Quel est le modèle de votre téléphone ?")
                .channel(Message.Channel.SMS)
                .author("Sonia Valentin")
                .build()));
        Assertions.assertEquals(1, messageService.findByAuthor("Sonia Valentin", Pageable.unpaged())
                .getNumberOfElements());
    }

    @Order(3)
    @Test
    public void attachSoniaMessageToJeremyRecord() {
        recordService.update(record.getId(), UpdateRecordDto.builder()
                .messages(messages.stream().map(Message::getId).collect(Collectors.toSet()))
                .reference(record.getReference())
                .clientName(record.getClientName())
                .build());

        System.out.println(recordService.get(Sets.newSet(record.getId())).iterator().next());
        Assertions.assertEquals(2,
                recordService.get(Sets.newSet(record.getId())).iterator().next().getMessages().size());
    }

    @Order(4)
    @Test
    public void updateRecordReference() {
        recordService.update(record.getId(), UpdateRecordDto.builder()
                .reference("KA-18B6")
                .clientName(record.getClientName())
                .messages(record.getMessages().stream().map(Message::getId).collect(Collectors.toSet()))
                .build());

        Assertions.assertEquals("KA-18B6",
                recordService.get(Sets.newSet(record.getId())).iterator().next().getReference());
    }

    @Order(5)
    @Test
    public void checkClientRecords() {
        Assertions.assertEquals(1, recordRepository.count());
    }
}
